/*
 * Asct launchpad-eXtend
 *
 * v0.31
 *
 */


Create custom lighting layouts for the Novation Launchpad easily. See the Docs folder for API info and the examples folder for example scripts.

This is still very much in beta, any issues please contact me via bitbucket or my website

The basis of using the script is to import the js from the 'template' folder (or make a copy of the template m4l device). Fill in the default values in the top part of the script.
The basics are set up in the function configSetup(), fill out the required values. Once this is done it will start to output data correctly when connected. 

If you need to write custom functions to handle the midi or data calls then you can do this in the functions midiproc() and dataproc(), to use these functions you must set config.midi.mode to "custom" or config.data.mode to "custom" in configSetup()

Most users probably dont need to go beyond line 335 as this is where the core of the code starts, all the lighting codes etc..... live here, so if you want to see how this works or you want to add your own lighting routines then add them below here and make sure you read the API docs to see how to integrate them properly

The examples show someways to configure the launchpad, either using custom data handling or using the default outs and dealing with the data in max