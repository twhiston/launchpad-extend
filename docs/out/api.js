YUI.add("yuidoc-meta", function(Y) {
   Y.YUIDoc = { meta: {
    "classes": [
        "asctCoreFunctions",
        "config",
        "launchpad-eXtend"
    ],
    "modules": [
        "launchpad-eXtend"
    ],
    "allModules": [
        {
            "displayName": "launchpad-eXtend",
            "name": "launchpad-eXtend",
            "description": "asct launchpad-eXtend\nVersion 0.31\nAll functions in this 'fake class' are related to data processing, input, output etc.... most are functions that the user will never need to call\nYou will see id and args used a lot as input, these are always the id of the callback with the data that it sent, these remain passed unedited to any function with these input arguments"
        }
    ]
} };
});