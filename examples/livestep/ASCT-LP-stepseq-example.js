/**
 * asct launchpad-eXtend
 * Version 0.31
 * All functions in this 'fake class' are related to data processing, input, output etc.... most are functions that the user will never need to call
 * You will see id and args used a lot as input, these are always the id of the callback with the data that it sent, these remain passed unedited to any function with these input arguments
 * @module launchpad-eXtend
 * @class launchpad-eXtend
 * @constructor
 */
/**
 * The config class is a 'fake class' to group together all of the functions and properties related to our program configuration
 * All the Properties are set in the configSetup() function. This is where you can describe your programs functionality and default settings
 * @class config
 * @constructor
 */
/**
 * Standard Max setup stuff
 */
inlets = 2;

setinletassist(0,"path in, use M4L.api.SelectControlSurface");
setoutletassist(0,"direct midi outlet");
setoutletassist(1,"data outlet");

var mtxX = 8;
var mtxY = 8;
var setupFlag = 0;
/*
 * Setup
 * This is wthe only bit of the script you need to touch to change functionality (unless you want custom midi out handling
 *
 * Configure the settings and functions appropriately and initialize 
 * 
 *
 * When the script is initialized in the recommended way setup will be called by the initialize function.
 * 
 */


/*
* The setup and dataproc below form a SIMPLE interaction with live.step
* This is intended as a basic demonstation of easy scripting for powerful custom control
* For a more fully featured implementation please see the live.step advanced example
*
* What this does:
* Setup as layers, each button on the right hand side calls a function which outputs data for live.set
* arm - midi note value
* solo - velocity
* trkOn - duration
* stop - extra1
* sndB - extra2
* sndA - loop lenght (please note in this basic implementation any row loop length will output to a single sequence in live.step) 
* pan - select mode TODO
* vol - n/a
*
* left/right = select next/prev note in seq
* up/down = change note value (very basic implementation with no awareness of current note, see advanced example for more)
*/

function configSetup(){
	
	config.buttonmode = "layers"; //can be "layers" or "latched" or "toggle"
	

	config.midi.mode = "direct"; 	//"direct" midi out will send midi directly using midi notes defined from
									//config.midi.mtx.root
									//config.midi.mtx.rootpos
									//config.buttons.top.midi
									//config.buttons.right.midi
									//"custom" midi out sends the midi data to the function 'midiproc()' for you to apply custom handling
	config.midi.mtx.root = 12; //root note for matrix, its up to you to make sure these dont clash with the siqe and top buttons midi calls!
	config.midi.mtx.rootpos = "top"; //value can be "top" or "bottom"
	config.buttons.top.midi = [60,61,62,63];//midi out values for top buttons L-R
	config.buttons.right.midi = [64,65,66,67,68,69,70,71]; //midi out values for side buttons top-bottom

	config.data.mode = "custom";//"custom" sends data to dataproc() for you to apply handling to
	
	//you only need to define the button mode you have chosen
	switch(config.buttonmode)
	{
	case "layers":

		config.up.func = "ud";
		config.down.func = "ud";
		config.left.func = "lr";
		config.right.func = "lr";
		
		config.vol.func = "writePads";
		config.pan.func = "writeRowLeft";
		config.sndA.func = "writeHorizontalLoop";
		config.sndB.func = "writeColumnBottom";
		config.stop.func = "writeColumnBottom";
		config.trkOn.func = "writeColumnBottom";
		config.solo.func = "writeColumnBottom";
		config.arm.func = "writeOnePerColumn";
		
		break;
	case "latched":
		break;
	case "toggle":
		break;
	}
	
}

/*
 * MIDIPROC
 * Add custom midi processing here
 * input is id of callback sending data and the full input arguments
 * output handling should be specified in the function
 * returns nothing
 */
function midiproc(id, args){
	
	//do something with your midi here and send it to an outlet, returns nothing
	
}

//basic version of dataproc for some fun with live.step
//as you can see most of the calls are very similar indeed and its really very fast to get this running
//all of the messages get formatted as the correct parameters and all info about these is in the live.step help

var loopstart;
function dataproc(id, args){

	//post("dataproc", id, args,"\n");
	
	
	//do something with your data here and send it to an outlet, returns nothing

	//we choose what to do based on the id of the data input
	switch(id)
	{
		//if the input is from the matrix what we do depends on which button on the right is active, which corresponds with the layers setting above
		case"mtx":
			switch(config.active)
			{
				case"arm":
					outlet(1, "pitch",args[2]+1,(config.midi.mtx.root+(7-args[3])));//send pitch message to live.step, midi values based on configured root
					break;
				case"solo":
					outlet(1, "velocity",args[2]+1, ( (7-args[3]) * (127/8) ) );//send velocity to live.step
					break;
				case"trkOn":
					outlet(1, "duration",args[2]+1, ( (7-args[3]) * (127/8) ) );//send velocity to live.step
					break;
				case"stop":
					outlet(1, "extra1",args[2]+1, ( (7-args[3]) * (127/8) ) );//send extra1 to live.step
					break;
				case"sndB":
					outlet(1, "extra2",args[2]+1, ( (7-args[3]) * (127/8) ) );//send extra2 to live.step
					break;
				case"sndA":
					//to do the loop is a little more complicated as we have to store the value that occurs on the red button being set
					//at the moment this only works on a single layer for clarity, see extended version of this example for layers implementation
					//note that whilst its valid to draw a backwards loop these do not output valid data for the live.step ayd additional handling would be required
					if(asctCoreFunctions['writeHorizontalLoop'].HR_init == 1 && args[1] != 0){
						loopstart = args[2]+1;//store the loop start value
					}
					if(asctCoreFunctions['writeHorizontalLoop'].HR_init == 0 && args[1] != 0){
						outlet(1, "loop", loopstart, args[2]+1 );
					}
					break;
				case"vol":

						outlet(1, "mode",asctCoreFunctions['writePads'].data[args[2]][args[3]]);
					break;
			}
		break;

		/*now we do some other actions for the navigation buttons
		 *this bit does a rough note edit, l and r move through notes, up and down change pitch
		 *this does not check for the current pitch so it jumps to whatever value is stored in as the row/col value 
		 *See stepseq-advanced example for how to deal with this more elegantly
		*/
		case"left":
			if(args[1] == 0)
				return;
			outlet(1, "left");
			break;
		case"right":
			if(args[1] == 0)
				return;
			outlet(1, "right");
			break;
		case"up":
			if(args[1] == 0)
				return;
			outlet(1, "up");
			break;
		case"down":
			if(args[1] == 0)
				return;
			outlet(1, "down");
			break;
	}	
	
	
}



/***********************************************
 * BASIC USERS CHANGE NOTHING BEYOND THIS LINE *
 ***********************************************/


/*
 * FUNCTIONS
 * 
 * below this point are functions you can call to do things with your input
 *
 * To add a new function attach it to the object below with a unique name
 * reference it using this name in the configSetup() function
 *
 */

/**
 * add any custom user function variable definitions or setup code to this function.
 * To add persistant variables to functions it is advisable to attach them to the function object 
 * by declaring them in this initialization routine, such as:
 * asctCoreFunctions['writeHorizontalLoop'].HR_oldrow = 8;
 * @method initializeCustomFunctions
 * @return {void} 
 * @for  config
 */
function initializeCustomFunctions()
{

}

/**
 * asctCoreFunctions object to hold all our lighting methods
 * we put the functions into this object so that we can call them by name later.
 * Custom user functions can be added to this section that you can then attach to a button via the setup routine
 * all user functions must be added to the main function array 
 *
 * Out of convention all function names start with "write"
 *
 * Functions handle all lighting calls and data saving internally, functions have been provided to assist with this 
 * Please see the helper functions section for these and study how they work in the core functions
 *
 *  Template for a custom function call
 *
 	asctCoreFunctions['writeFunctionNameHere'] = function(id, args){
 		//do stuff here such as
		lightOn(LP.mtx, args[2], args[3], 60); //turn on a light in the matrix
		saveMtxRef(config.active,args[2], args[3],60); //save the lighting call into the associated layers data array
 	}
 * id and args are the standard input, these are always the id of the callback and the data that it sent
 * LightOn and saveMtxRef are 2 functions you will use over and over in creating functions. They are in use in every core functions  
 * when saving a matrix reference make the last value of the lighting call the light colour because when the matrix is reloaded it will
 * send these values to the lighting call.
 * @type {Object}
 * @class asctCoreFunctions
 * @constructor
 */
asctCoreFunctions = new Object();

/**************
 * User Functions
 * Add some user functions here if you want
  **************/


/**************
 * Matrix Functions
 **************/

/**
 * this function only allows one light to be lit per row, useful for panning etc....
 * @method  writeOnePerRow
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {void}     
 */
asctCoreFunctions['writeOnePerRow'] = function(id, args){
	
	if(args[1] == 0){
		//if input was a 0 output is null so kill process
		return;
	}
	
	post("one per row: ", args, "\n");
	
	for (var i = 0; i < 8; i++) {
		if (i == args[2]) {
			lightOn(LP.mtx, args[2], args[3], 60);
			saveMtxRef(config.active,args[2], args[3],60);
		} else {
			lightOff(LP.mtx, i, args[3]);
			saveMtxRef(config.active,i, args[3],12);
		}
	}
	
}
/**
 * useful for a step sequencer like live.step note entry
 * @method writeOnePerColumn
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['writeOnePerColumn'] = function(id, args){
	
	if(args[1] == 0){
		//if input was a 0 output is null so kill process
		return;
	}
	
	post("one per row: ", args, "\n");
	
	for (var i = 0; i < 8; i++) {
		if (i == args[3]) {
			lightOn(LP.mtx, args[2], args[3], 60);
			saveMtxRef(config.active,args[2], args[3],60);
		} else {
			lightOff(LP.mtx, args[2], i);
			saveMtxRef(config.active,args[2], i,12);
		}
	}
	
}
/**
 * Define a start and end point of a horizontal loop. One per row.
 * Useful for live.step loop lengths
 * @method writeHorizontalLoop
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['writeHorizontalLoop'] = function(id, args){
	

	if(args[1] == 0){
		//if input was a 0 output is null so kill process
		return;
	}

	//get col and row vals
	var Hcol = args[2];
	var Hrow = args[3];
	//if row is different from last row make it red in the db and add a lighting call
	if (asctCoreFunctions['writeHorizontalLoop'].HR_oldrow != Hrow){
		//post('old row does not equal new row. OLD: '+HR_oldrow+" current row"+a[1]+'\n');
		//write in new lighting call
		for(var i = 0;i<8;i++){
			if(i == Hcol){
				lightOn(LP.mtx, Hcol, Hrow, 15);
			} else {
				lightOff(LP.mtx, i, Hrow);
			}
		}
		//set our old row to be the row we just hit
		asctCoreFunctions['writeHorizontalLoop'].HR_init = 0;
		asctCoreFunctions['writeHorizontalLoop'].HR_oldrow = Hrow;
		asctCoreFunctions['writeHorizontalLoop'].HR_oldcol = Hcol;
	} else {
		//if row is the same calculate the difference between the two and set them in the db
		//post('old row equals new row. OLD: '+HR_oldrow+" current row"+a[1]+'\n');
		//post('old col: '+HR_oldcol+" new col: "+a[0]+'\n');
		if( Hcol > asctCoreFunctions['writeHorizontalLoop'].HR_oldcol){
			for (var i = 0; i < 8; i++) {
			//write in new values dependant on if it falls within our range or not
				if (i >= asctCoreFunctions['writeHorizontalLoop'].HR_oldcol && i <= Hcol) {
					lightOn(LP.mtx, i, Hrow, 60);
					saveMtxRef(config.active,i,Hrow,60);
				} else {
					lightOff(LP.mtx, i, Hrow);
					saveMtxRef(config.active,i,Hrow,12);
				}
			}
		}
		if( Hcol < asctCoreFunctions['writeHorizontalLoop'].HR_oldcol){
			for (var i = 0; i < 8; i++) {
			//write in new values dependant on if it falls within our range or not
				if (i <= asctCoreFunctions['writeHorizontalLoop'].HR_oldcol && i >= Hcol) {
					lightOn(LP.mtx, i, Hrow, 60);
					saveMtxRef(config.active,i,Hrow,60);
				} else {
					lightOff(LP.mtx, i, Hrow);
					saveMtxRef(config.active,i,Hrow,12);
				}
			}
		}
		//set our current rows to unobtainable number so we can alter the same row again
		asctCoreFunctions['writeHorizontalLoop'].HR_oldrow = 8;
		asctCoreFunctions['writeHorizontalLoop'].HR_oldcol = 8;	
		asctCoreFunctions['writeHorizontalLoop'].HR_init = 1;
	}
}


/**
 * like horizontal loop but one per column
 * @method writeVerticalLoop
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['writeVerticalLoop'] = function(id, args){
	
	
//This does not do any data out yet
	if(args[1] == 0){
		//if input was a 0 output is null so kill process
		return;
	}
	//post('HORIZONTAL LOOP \n');
	//get col and row vals
	var Hcol = args[2];
	var Hrow = args[3];
	//if row is different from last row make it red in the db and add a lighting call
	if (asctCoreFunctions['writeVerticalLoop'].HR_oldcol != Hcol){
		post("changed col \n");
		//post('old row does not equal new row. OLD: '+HR_oldrow+" current row"+a[1]+'\n');
		//write in new lighting call
		for(var i = 0;i<8;i++){
			if(i == Hrow){
				lightOn(LP.mtx, Hcol, Hrow, 15);
			} else {
				lightOff(LP.mtx, Hcol, i);
			}
		}
		//set our old row to be the row we just hit
		asctCoreFunctions['writeVerticalLoop'].HR_oldrow = Hrow;
		asctCoreFunctions['writeVerticalLoop'].HR_oldcol = Hcol;
	} else {

		//if row is the same calculate the difference between the two and set them in the db
		//post('old row equals new row. OLD: '+HR_oldrow+" current row"+a[1]+'\n');
		//post('old col: '+HR_oldcol+" new col: "+a[0]+'\n');
		if( Hrow > asctCoreFunctions['writeVerticalLoop'].HR_oldrow){
			for (var i = 0; i < 8; i++) {
			//write in new values dependant on if it falls within our range or not
				if (i >= asctCoreFunctions['writeVerticalLoop'].HR_oldrow && i <= Hrow) {
					lightOn(LP.mtx, Hcol, i, 60);
					saveMtxRef(config.active,Hcol,i,60);
				} else {
					lightOff(LP.mtx, Hcol, i);
					saveMtxRef(config.active,Hcol,i,12);
				}
			}
		} else if( Hrow < asctCoreFunctions['writeVerticalLoop'].HR_oldrow){
			for (var i = 0; i < 8; i++) {
			//write in new values dependant on if it falls within our range or not
				if (i <= asctCoreFunctions['writeVerticalLoop'].HR_oldrow && i >= Hrow) {
					lightOn(LP.mtx, Hcol, i, 60);
					saveMtxRef(config.active,Hcol,i,60);
				} else {
					lightOff(LP.mtx, Hcol, i);
					saveMtxRef(config.active,Hcol,i,12);
				}
			}
			
		}
		//set our current rows to unobtainable number so we can alter the same row again
		asctCoreFunctions['writeVerticalLoop'].HR_oldrow = 8;
		asctCoreFunctions['writeVerticalLoop'].HR_oldcol = 8;	
	}
}

/**
 * column up from bottom, useful for a fader, eq etc....
 * @method  writeColumnBottom
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['writeColumnBottom'] = function (id, args){
//a[0] is col a[1] is row
	if(args[1] = 0){
		//if input was a 0 output is null so kill process
		return;
	}
//go through or 8 calls, if the number is less than/= to the row then make it green
for (var i = 0; i < 8; i++) {
	if (i >= args[3]) {
		lightOn(LP.mtx, args[2], i, 60);
		saveMtxRef(config.active,args[2],i,60);
	} else {
		lightOff(LP.mtx, args[2], i);
		saveMtxRef(config.active,args[2],i,12);
	}

}
}
/**
 * same as writeColumnBottom but starts from the top
 * @method writeColumnTop
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['writeColumnTop'] = function (id, args){
//a[0] is col a[1] is row
	if(args[1] = 0){
		//if input was a 0 output is null so kill process
		return;
	}
//go through or 8 calls, if the number is less than/= to the row then make it green
for (var i = 0; i < 8; i++) {
	if (i <= args[3]) {
		lightOn(LP.mtx, args[2], i, 60);
		saveMtxRef(config.active,args[2],i,60);
	} else {
		lightOff(LP.mtx, args[2], i);
		saveMtxRef(config.active,args[2],i,12);
	}

}
}
/**
 * light up rhs to x per row
 * @method writeRowRight
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['writeRowRight'] = function (id, args){
	if(args[1] = 0){return;}
//go through or 8 calls, if the number is less than/= to the row then make it green
for (var i = 0; i < 8; i++) {
	if (i >= args[2]) {
		lightOn(LP.mtx, i, args[3], 60);
		saveMtxRef(config.active,i,args[3],60);
	} else {
		lightOff(LP.mtx, i, args[3]);
		saveMtxRef(config.active,i,args[3],12);
	}

}
}
/**
 * light up lhs to x per row
 * @method writeRowLeft
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['writeRowLeft'] = function (id, args){
	if(args[1] = 0){return;}
//go through or 8 calls, if the number is less than/= to the row then make it green
for (var i = 0; i < 8; i++) {
	if (i <= args[2]) {
		lightOn(LP.mtx, i, args[3], 60);
		saveMtxRef(config.active,i,args[3],60);
	} else {
		lightOff(LP.mtx, i, args[3]);
		saveMtxRef(config.active,i,args[3],12);
	}

}
}

/**
 * make some pads
 * @method writePads
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['writePads'] = function (id, args){

	//if the function runs and the data array hasnt been set up then trigger a setup and change the flag
	if(asctCoreFunctions['writePads'].initFlag == 0){
		asctCoreFunctions['writePads'].init();
		asctCoreFunctions['writePads'].initFlag = 1;
	}


}

asctCoreFunctions['writePads'].init = function (){

	//make an array with the right values in it based on the pad size variable
	var xcount = 0;
	 for(var x = 0;x<mtxX;x++){
	 	var totalcount = xcount;
	 	for(var y = 0; y<mtxY;y++){
	 		lightOn(LP.mtx, x, y, asctCoreFunctions['writePads'].colors[totalcount]);
			saveMtxRef(config.active,x,y,asctCoreFunctions['writePads'].colors[totalcount]);
			asctCoreFunctions['writePads'].data[x][y] = totalcount;
	 		if((y+1)%(asctCoreFunctions['writePads'].padsize) == 0 && y != 0){
	 			(totalcount==asctCoreFunctions['writePads'].colors.length-1)?totalcount=0:totalcount++;
	 		}
	 	}
	 	if((x+1)%(asctCoreFunctions['writePads'].padsize) == 0 && y != 0){
	 		((xcount+1)==asctCoreFunctions['writePads'].colors.length)?xcount=0:xcount=totalcount;
	 	}
	 	
	 }
}

/**
 * toggle 1 color
 * @method  write1ClMtx
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['write1ClMtx'] = function (id, args){
	if(args[1] = 0){return;}
	var curval = getMtxRef(config.active, args[2], args[3]);
	if(curval == 0){
		lightOn(LP.mtx, args[2], args[3], 60);
		saveMtxRef(config.active,args[2], args[3],60);	
	} else {
		lightOff(LP.mtx, args[2], args[3]);
		saveMtxRef(config.active,args[2], args[3],0);	
	}
}
/**
 * toggle 2 colors
 * @method  write2ClMtx
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['write2ClMtx'] = function (id, args){
	if(args[1] = 0){return;}
	var curval = getMtxRef(config.active, args[2], args[3]);
	if(curval == 0){
		lightOn(LP.mtx, args[2], args[3], 60);
		saveMtxRef(config.active,args[2], args[3],60);	
	} else if(curval == 60){
		lightOn(LP.mtx, args[2], args[3], 63);
		saveMtxRef(config.active,args[2], args[3],63);
	} else {
		lightOff(LP.mtx, args[2], args[3]);
		saveMtxRef(config.active,args[2], args[3],0);	
	}
}
/**
 * toggle 3 colors
 * @method  write3ClMtx
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['write3ClMtx'] = function (id, args){
	if(args[1] = 0){return;}
	var curval = getMtxRef(config.active, args[2], args[3]);
	if(curval == 0){
		lightOn(LP.mtx, args[2], args[3], 60);
		saveMtxRef(config.active,args[2], args[3],60);	
	} else if(curval == 60){
		lightOn(LP.mtx, args[2], args[3], 63);
		saveMtxRef(config.active,args[2], args[3],63);
	} else if(curval == 63){
		lightOn(LP.mtx, args[2], args[3], 15);
		saveMtxRef(config.active,args[2], args[3],15);	
	} else {
		lightOff(LP.mtx, args[2], args[3]);
		saveMtxRef(config.active,args[2], args[3],0);	
	}
}

/**
 * toggle 1 color row
 * @method  write1ClRow
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['write1ClRow'] = function (id, args){
	if(args[1] = 0){return;}
	var curval = getMtxRef(config.active, args[2], args[3]);
	if(curval == 0){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, i, args[3], 60);
			saveMtxRef(config.active,i, args[3],60);	
		}
	} else {
		for(var i = 0; i < mtxX;i++){
			lightOff(LP.mtx, i, args[3]);
			saveMtxRef(config.active,i, args[3],0);	
		}
	}
}
/**
 * toggle 2 colors row
 * @method  write2ClRow
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['write2ClRow'] = function (id, args){
	if(args[1] = 0){return;}
	var curval = getMtxRef(config.active, args[2], args[3]);
	if(curval == 0){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, i, args[3], 60);
			saveMtxRef(config.active,i, args[3],60);	
		}
	} else if(curval == 60){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, i, args[3], 63);
			saveMtxRef(config.active,i, args[3],63);
		}
	} else {
		for(var i = 0; i < mtxX;i++){
			lightOff(LP.mtx, i, args[3]);
			saveMtxRef(config.active,i, args[3],0);
		}	
	}
}

/**
 * toggle 3 colors row
 * @method  write3ClRow
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['write3ClRow'] = function (id, args){
	if(args[1] = 0){return;}
	var curval = getMtxRef(config.active, args[2], args[3]);
	if(curval == 0){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, i, args[3], 60);
			saveMtxRef(config.active,i, args[3],60);	
		}
	} else if(curval == 60){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, i, args[3], 63);
			saveMtxRef(config.active,i, args[3],63);
		}
	} else if(curval == 63){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, i, args[3], 15);
			saveMtxRef(config.active,i, args[3],15);	
		}
	} else {
		for(var i = 0; i < mtxX;i++){
			lightOff(LP.mtx, i, args[3]);
			saveMtxRef(config.active,i, args[3],0);	
		}
	}
}

/**
 * toggle 1 color col
 * @method  write1ClCol
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['write1ClCol'] = function (id, args){
	if(args[1] = 0){return;}
	var curval = getMtxRef(config.active, args[2], args[3]);
	if(curval == 0){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, args[2], i, 60);
			saveMtxRef(config.active,args[2],i,60);	
		}
	} else {
		for(var i = 0; i < mtxX;i++){
			lightOff(LP.mtx, args[2], i);
			saveMtxRef(config.active,args[2],i, 0);	
		}
	}
}
/**
 * toggle 2 colors row
 * @method  write2ClCol
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['write2ClCol'] = function (id, args){
	if(args[1] = 0){return;}
	var curval = getMtxRef(config.active, args[2], args[3]);
	post("curval",curval,"\n");
	if(curval == 0){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, args[2],i, 60);
			saveMtxRef(config.active,args[2],i,60);	
		}
	} else if(curval == 60){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, args[2],i, 63);
			saveMtxRef(config.active,args[2],i,63);
		}
	} else {
		for(var i = 0; i < mtxX;i++){
			lightOff(LP.mtx,args[2],i);
			saveMtxRef(config.active,args[2],i,0);
		}	
	}
}

/**
 * toggle 3 colors col
 * @method  write3ClRow
 * @param  {[type]} id   [description]
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['write3ClCol'] = function (id, args){
	if(args[1] = 0){return;}
	var curval = getMtxRef(config.active, args[2], args[3]);
	if(curval == 0){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, args[2], i, 60);
			saveMtxRef(config.active,args[2], i,60);	
		}
	} else if(curval == 60){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, args[2], i, 63);
			saveMtxRef(config.active,args[2], i,63);
		}
	} else if(curval == 63){
		for(var i = 0; i < mtxX;i++){
			lightOn(LP.mtx, args[2], i, 15);
			saveMtxRef(config.active,args[2], i,15);	
		}
	} else {
		for(var i = 0; i < mtxX;i++){
			lightOff(LP.mtx, args[2], i);
			saveMtxRef(config.active,args[2], i,0);	
		}
	}
}

// END OF Matrix Functions

/**************
 * Nav button Functions
 **************/

/*
 config.down.func = "udlit";
		config.left.func
*/
/**
 * light the up or down button
 * @method udlit
 * @param  {[type]} id   "up" or "down"
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['udlit'] = function (id, args){

	//post("udlcount: ", asctCoreFunctions['udlit'].udlcount,"\n");

	(config.left.func == "lrlit" && config.right.func == "lrlit")?asctCoreFunctions['udlit'].lrflag=1:asctCoreFunctions['udlit'].lrflag=0;

	for(var i = 0; i<8;i++){
		if (asctCoreFunctions['udlit'].lrflag == 1) {
			if(i != asctCoreFunctions['lrlit'].lrlcount)
				lightOff(LP.mtx,i, asctCoreFunctions['udlit'].udlcount);
		} else {
			lightOff(LP.mtx,i, asctCoreFunctions['udlit'].udlcount);
		}
	}
		

	if(id == "up"){
		(asctCoreFunctions['udlit'].udlcount > 0)?asctCoreFunctions['udlit'].udlcount--:asctCoreFunctions['udlit'].udlcount = 7;
	} else if (id == "down"){
		(asctCoreFunctions['udlit'].udlcount< mtxY-1)?asctCoreFunctions['udlit'].udlcount++:asctCoreFunctions['udlit'].udlcount = 0;
	}
	

	for(var i = 0; i<8;i++)
		lightOn(LP.mtx,i, asctCoreFunctions['udlit'].udlcount,63);


}

/**
 * light the l or r button
 * @method lrlit
 * @param  {[type]} id   left or right
 * @param  {[type]} args [description]
 * @return {[type]}      [description]
 */
asctCoreFunctions['lrlit'] = function (id, args){

	//post("lrlcount: ", asctCoreFunctions['lrlit'].lrlcount,"\n");

	(config.up.func == "udlit" && config.down.func == "udlit")?asctCoreFunctions['lrlit'].udflag=1:asctCoreFunctions['lrlit'].udflag=0;

	for(var i = 0; i<8;i++){
		if (asctCoreFunctions['lrlit'].udflag == 1) {
			if(i != asctCoreFunctions['udlit'].udlcount)
				lightOff(LP.mtx,asctCoreFunctions['lrlit'].lrlcount, i);
		} else {
			lightOff(LP.mtx,asctCoreFunctions['lrlit'].lrlcount, i);
		}
	}

	if(id == "left"){
		(asctCoreFunctions['lrlit'].lrlcount > 0)?asctCoreFunctions['lrlit'].lrlcount--:asctCoreFunctions['lrlit'].lrlcount = 7;
	} else if (id == "right"){
		(asctCoreFunctions['lrlit'].lrlcount< mtxY-1)?asctCoreFunctions['lrlit'].lrlcount++:asctCoreFunctions['lrlit'].lrlcount = 0;
	}
	

	for(var i = 0; i<8;i++)
		lightOn(LP.mtx,asctCoreFunctions['lrlit'].lrlcount, i,63);


}

var lrcount = 0;
asctCoreFunctions['lr'] = function (id, args){

	if(id == "left"){
		(lrcount > 0)?lrcount--:lrcount = 0;
	} else if (id == "right"){
		(lrcount>= 0)?lrcount++:lrcount = 0;
	}
}

var udcount = 0;
asctCoreFunctions['ud'] = function (id, args){

	if(id == "down"){
		(udcount > 0)?udcount--:udcount = 0;
	} else if (id == "up"){
		(udcount>= 0)?udcount++:udcount = 0;
	}

}



/*
 * LIGHTING FUNCTIONS
 * Basic Lighting functions
 * Includes static color calls for ease of use as well as dynamic
 * Overloaded generic functions below
 */

/**
 * clear all the matrix lights
 * @for launchpad-eXtend
 * @method  clearmtx
 * @return {[type]} [description]
 */
function clearmtx()
{
	for(var i = 0; i < 8;i++){
			for(var a = 0; a < 8; a++){
				lightOff(LP.mtx, i, a);
			}
	}
}

/**
 * Helper function to clear the nav buttons by name
 * @method  clearNav 
 * @param  {String} id name button names - up, down, left, right
 * @return {void} 
 */
function clearNav(id){
	if (id != "up")
		lightOffRef("up");
	if (id != "down")
		lightOffRef("down");
	if (id != "left")
		lightOffRef("left");
	if (id != "right")
		lightOffRef("right");
}

/**
 * Turn off all buttons on the right hand side
 * @method  clearrhs		
 * @return {void}
 */
function clearrhs()
{
	LP.vol.call("send_value", 12);
	LP.pan.call("send_value", 12);
	LP.sndA.call("send_value", 12); 
	LP.sndB.call("send_value", 12); 
	LP.stop.call("send_value", 12); 
	LP.trkOn.call("send_value", 12);
	LP.solo.call("send_value", 12); 
	LP.arm.call("send_value", 12);
}

/**
 * Turn off all buttons in the top 4 nav buttons
 * @method  cleartop
 * @return {void} 
 */
function cleartop()
{
	LP.up.call("send_value", 12);
	LP.down.call("send_value", 12);
	LP.left.call("send_value", 12);
	LP.right.call("send_value", 12);
}

/**
 * loads a matrix of lighting values
 * @method  loadMtx
 * @param  {Object} target something like LP.arm.data
 * @return {void}     sets a lighting state, no return
 */
function loadMtx(target)
{
	for(var i = 0;i<mtxX;i++){
		for(var a = 0;a<mtxY;a++){
			LP.mtx.call("send_value", i , a, target[i][a]);
		}
	}
}

/**
 * takes a reference and sends the corresponding object to loadMtx
 * @method  loadMtxRef
 * @param  {String} ref named reference to the called such as "mtx" or "vol"
 * @return {void}    
 */
function loadMtxRef(ref)
{	
	post('loadMtxRef', ref,"\n");
	switch(ref)
		{
			case "mtx":
				break;
			case "vol":
				loadMtx(config.vol.data);
				break;
			case "pan":
				loadMtx(config.pan.data);
				break
			case "sndA":
				loadMtx(config.sndA.data);
				break;
			case "sndB":
				loadMtx(config.sndB.data);
				break;
			case "stop":
				loadMtx(config.stop.data);
				break;
			case "trkOn":
				loadMtx(config.trkOn.data);
				break;
			case "solo":
				loadMtx(config.solo.data);
				break;
			case "arm":
				loadMtx(config.arm.data);
				break;
			case "up":
				loadMtx(config.up.data);
				break;
			case "down":
				loadMtx(config.down.data);
				break;
			case "left":
				loadMtx(config.left.data);
				break;
			case "right":
				loadMtx(config.right.data);
				break;		
		}
}

/**
 * @method  lightOn 
 * @param  {Object} obj  object to send the call to 
 * @param  {[type]} value 
 * @return {[type]}      
 */
function lightOn(obj, value)
{
		obj.call("send_value", value);
}
 /**
  * @method  lightOn 
  * @param  {Object} obj   object to send the call to 
  * @param  {int} col   column
  * @param  {int} row   row
  * @param  {int} value color value
  * @return {void} 
  */
function lightOn(obj, col, row, value)
{
		obj.call("send_value", col, row, value);
}
/**
 * turn off all the lights based on passing an object
 * @method lightOff
 * @param  {[type]} obj [description]
 * @return {[type]}     [description]
 */
function lightOff(obj)
{		
		obj.call("send_value", 12);
}
/**
 * overloaded lightOff where you can specify a col and row value
 * @lightOff
 * @param  {[type]} obj [description]
 * @param  {[type]} col [description]
 * @param  {[type]} row [description]
 * @return {[type]}     [description]
 */
function lightOff(obj, col, row)
{
		obj.call("send_value",col,row,12);
}


/**
 * takes a reference "mtx", "vol", "pan" etc.... and calls a light off with it. Will clear the whole row by calling lightOff(obj)
 * @method  lightOffRef
 * @param  {[type]} ref [description]
 * @return {void}    
 */
function lightOffRef(ref)
{		
	post("lightOffRef",ref,"\n");
	switch(ref)
		{
			case "mtx":
				break;
			case "vol":
				lightOff(LP.vol);
				break;
			case "pan":
				lightOff(LP.pan);
				break
			case "sndA":
				lightOff(LP.sndA);
				break;
			case "sndB":
				lightOff(LP.sndB);
			case "stop":
				lightOff(LP.stop);
				break;
			case "trkOn":
				lightOff(LP.trkOn);
				break;
			case "solo":
				lightOff(LP.solo);
				break;
			case "arm":
				lightOff(LP.arm);
				break;
			case "up":
				lightOff(LP.up);
				break;
			case "down":
				lightOff(LP.down);
				break;
			case "left":
				lightOff(LP.left);
				break;
			case "right":
				lightOff(LP.right);
				break;		
		}
}




/*
 * ALL FUNCTIONS FOR LIGHTING BELOW THIS POINT TAKE FULL ARGS ARRAYS FROM INPUT. IF YOU WANT TO SPECIFY A VALUE DIRECTLY USE FUNCTIONS ABOVE AND NOT THESE
 */

/**
 * quicktoggle green. Takes full array of input arguments
 * @method toggleGreen
 * @param  {Object} obj  object to send the call to
 * @param  {Array} args full array of input arguments
 * @return {void}      
 */
function toggleGreen(obj, args)
{
		(args[1] == 127)?obj.call("send_value", 60):obj.call("send_value", 12);
}
/**
 * quicktoggle orange. Takes full array of input arguments
 * @method toggleOrange
 * @param  {Object} obj  object to send the call to
 * @param  {Array} args full array of input arguments
 * @return {void}      
 */
function toggleOrange(obj, args)
{
		(args[1] == 127)?obj.call("send_value", 63):obj.call("send_value", 12);
}
/**
 * quicktoggle red. Takes full array of input arguments
 * @method toggleRed
 * @param  {Object} obj  object to send the call to
 * @param  {Array} args full array of input arguments
 * @return {void}      
 */
function toggleRed(obj, args)
{
		(args[1] == 127)?obj.call("send_value", 15):obj.call("send_value", 12);
}


/**
 * quicktoggle matrix green. Takes full array of input arguments
 * @method mtxToggleGreen
 * @param  {Object} obj  object to send the call to
 * @param  {Array} args full array of input arguments
 * @return {void}      
 */
function mtxToggleGreen(obj, args)
{
	(args[1] == 127)?obj.call("send_value",args[2],args[3],60):obj.call("send_value",args[2],args[3],12);
}
/**
 * quicktoggle matrix orange. Takes full array of input arguments
 * @method mtxToggleOrange
 * @param  {Object} obj  object to send the call to
 * @param  {Array} args full array of input arguments
 * @return {void}      
 */
function mtxToggleOrange(obj, args)
{
	(args[1] == 127)?obj.call("send_value",args[2],args[3],63):obj.call("send_value",args[2],args[3],12);
}
/**
 * quicktoggle matrix red. Takes full array of input arguments
 * @method mtxToggleRed
 * @param  {Object} obj  object to send the call to
 * @param  {Array} args full array of input arguments
 * @return {void}      
 */
function mtxToggleRed(obj, args)
{
	(args[1] == 127)?obj.call("send_value",args[2],args[3],15):obj.call("send_value",args[2],args[3],12);
}

/**
 * @method toggleAny
 * @param  {Object} obj  object to send the call to
 * @param  {Array} args full array of input arguments
 * @param  {Int} col  color value to set button to
 * @return {void}      
 */
function toggleAny(obj, args, col)
{
		(args[1] == 127)?obj.call("send_value", col):obj.call("send_value", 12);
}

/**
 * @method mtxToggleAny
 * @param  {Object} obj  object to send the call to
 * @param  {Array} args full array of input arguments
 * @param  {Int} col  color value to set button to
 * @return {void}      
 */
function mtxToggleAny(obj, args, col)
{
		(args[1] == 127)?obj.call("send_value",args[2],args[3], col):obj.call("send_value", args[2],args[3],12);
}



/*
 * PEEP FUNCTIONS
 * 
 * These are callbacks attached to the buttons that perform actions
 * We use these to attach a caller id to each button press and sent them on to grand central station for processing
 */
function mtxPeep(args)
{
	post("MTX PEEP \n");
	gcs("mtx",args);
}

function volPeep(args)
{
	gcs("vol",args);
}

function panPeep(args)
{
	gcs("pan",args);
}

function sndAPeep(args)
{
	gcs("sndA",args);
}

function sndBPeep(args)
{
	gcs("sndB",args);
}

function stopPeep(args)
{
	gcs("stop",args);
}

function trkOnPeep(args)
{
	gcs("trkOn",args);
}

function soloPeep(args)
{
	gcs("solo",args);
}

function armPeep(args)
{
	gcs("arm",args);
}

function upPeep(args)
{
	gcs("up",args);
}

function downPeep(args)
{
	gcs("down",args);
}

function leftPeep(args)
{
	gcs("left",args);
}

function rightPeep(args)
{
	gcs("right",args);
}
function sessPeep(args)
{
	gcs("sess",args);
}
function u1Peep(args)
{
	gcs("user1",args);
}
function u2Peep(args)
{
	gcs("user2",args);
}
function mixPeep(args)
{
	gcs("mixer",args);
}

/**
 * initialize makes all the api objects we need to attach to our global LP object. 
 * initialize MUST be called for anything to work
 * @method initialize 
 * @return {void} 
 */
function initialize()
{
	if (typeof LP !== 'undefined') {
		post("deleting existing launchpad instance \n");
        delete LP;
      }
	//initialize the launchpad object and listeners
	initializeLaunchpad();
	post("LaunchPad Initialized \n");

	if (typeof config !== 'undefined') {
		post("deleting existing config instance \n");
        delete config;
      }
	
	initializeConfig();
	post("config initialized \n");
	
	//we should have a setup function for modules that can set this stuff
	initializeCoreFunctions();
	initializeCustomFunctions();
	

	configSetup(); //setup the users config

	//clear all the displays
	clearmtx();
	clearrhs();
	cleartop();

	//set arm as the default channel
	config.active = "arm";
	config.activefunc = config.arm.func;
	lightOn(LP.arm, 60);

	//finish setup
	setupFlag = 1;
	post("Setup Done \n");
	
}
/**
 * Called as part of the initialize routine
 * Creates the launchpad object, attaches all the listeners
 * @method initializeLaunchpad
 * @return {void} [description]
 * @private
 */
function initializeLaunchpad()
{
	LP = new Object();
	LP.path = tempPath;
	
	//attach all our callbacks
	LP.mtx = new LiveAPI(mtxPeep,LP.path+" controls 1");
	
	LP.up = new LiveAPI(upPeep,LP.path+" controls 67");
	LP.down = new LiveAPI(downPeep,LP.path+" controls 68");
	LP.left = new LiveAPI(leftPeep,LP.path+" controls 69");
	LP.right = new LiveAPI(rightPeep,LP.path+" controls 70");
	LP.session = new LiveAPI(sessPeep,LP.path+" controls 71");
	LP.u1 = new LiveAPI(u1Peep,LP.path+" controls 72");
	LP.u2 = new LiveAPI(u2Peep,LP.path+" controls 73");
	LP.mix = new LiveAPI(mixPeep,LP.path+" controls 74");

	LP.vol = new LiveAPI(volPeep,LP.path+" controls 75");
	LP.pan = new LiveAPI(panPeep,LP.path+" controls 76");
	LP.sndA = new LiveAPI(sndAPeep,LP.path+" controls 77");
	LP.sndB = new LiveAPI(sndBPeep,LP.path+" controls 78");
	LP.stop = new LiveAPI(stopPeep,LP.path+" controls 79");
	LP.trkOn = new LiveAPI(trkOnPeep,LP.path+" controls 80");
	LP.solo = new LiveAPI(soloPeep,LP.path+" controls 81");
	LP.arm = new LiveAPI(armPeep,LP.path+" controls 82");
	

	LP.mtx.property = "value";
	
	LP.up.property = "value";
	LP.down.property = "value";
	LP.left.property = "value";
	LP.right.property = "value";

	LP.session.property = "value";
	LP.u1.property = "value";
	LP.u2.property = "value";
	LP.mix.property = "value";
	
	LP.vol.property = "value";
	LP.pan.property = "value";
	LP.sndA.property = "value";
	LP.sndB.property = "value";
	LP.stop.property = "value";
	LP.trkOn.property = "value";
	LP.solo.property = "value";
	LP.arm.property = "value";

	return;
}
initializeLaunchpad.local = 1;

/**
 * Internal function to initialize the config, called by initialize()
 * creates the config structure and creates data for it
 * @for config
 * @method  initializeConfig
 * @return {void}
 * @private
 */
function initializeConfig(){
	config = new Object();
	
	config.midi = new Object();
	config.midi.mtx = new Object();
	config.buttons = new Object();
	config.buttons.top = new Object();
	config.buttons.right = new Object();
	config.buttons.top.midi = new Array([4]);
	config.buttons.right.midi = new Array([8]);

	config.data = new Object();
	
	config.up = new Object();
	config.down = new Object();
	config.left = new Object();
	config.right = new Object();
	
	config.vol = new Object();
	config.pan = new Object();
	config.sndA = new Object();
	config.sndB = new Object();
	config.stop = new Object();
	config.trkOn = new Object();
	config.solo = new Object();
	config.arm = new Object();
	
	dataCreate(config.vol ,mtxX ,mtxY);
	dataCreate(config.pan ,mtxX ,mtxY);
	dataCreate(config.sndA ,mtxX ,mtxY);
	dataCreate(config.sndB ,mtxX ,mtxY);
	dataCreate(config.stop ,mtxX ,mtxY);
	dataCreate(config.trkOn ,mtxX ,mtxY);
	dataCreate(config.solo ,mtxX ,mtxY);
	dataCreate(config.arm ,mtxX ,mtxY);
	
	config.active = "arm";//set a default for the active layer

	return;
}
initializeConfig.local = 1;


/**
 * setup some defaults needed for the core functions. 
 * Users should add their own setup routines and initializations for custom functions to initializeCustomFunctions()
 * @method  initializeCoreFunctions
 * @return {void} 
 * @for  asctCoreFunctions
 * @private
 */
function initializeCoreFunctions()
{
	asctCoreFunctions['writeHorizontalLoop'].HR_oldrow = 8;
	asctCoreFunctions['writeHorizontalLoop'].HR_oldcol = 8;
	asctCoreFunctions['writeVerticalLoop'].HR_oldrow = 8;
	asctCoreFunctions['writeVerticalLoop'].HR_oldcol = 8;
	asctCoreFunctions['writeHorizontalLoop'].HR_init = 1;

	asctCoreFunctions['udlit'].udlcount = 0;
	asctCoreFunctions['udlit'].lrflag = 0;

	asctCoreFunctions['lrlit'].lrlcount = 0;
	asctCoreFunctions['lrlit'].udflag = 0;

	asctCoreFunctions['writePads'].padsize = 4;
	asctCoreFunctions['writePads'].initFlag = 0;
	asctCoreFunctions['writePads'].colors = [60,15,63];
	asctCoreFunctions['writePads'].data = Create2DArray(mtxX);

	return;
}
initializeCoreFunctions.local = 1;
/**
 * @method  dataCreate 
 * @for  config
 * @param  {[type]} obj [description]
 * @param  {[type]} x   [description]
 * @param  {[type]} y   [description]
 * @return {[type]}     [description]
 */
function dataCreate(obj, x, y)
{
	obj.data = new Array([]);

	for(var i = 0;i<x;i++){
		obj.data[i] = new Array();
		for(var a = 0; a < y; a++){
			obj.data[i][a] = 0;
		}
	}
}


/**
 * saves the matrix state to the associated call buttons data array
 * @method saveMtx
 * @for  launchpad-eXtend
 * @param  {[type]} target config structure config.active
 * @param  {[type]} x      [description]
 * @param  {[type]} y      [description]
 * @param  {[type]} v      [description]
 * @return {[type]}        [description]
 */
function saveMtx(target, x, y, v)
{
	target[x][y] = v;
}

/**
 * takes a name and some data and matches it to the correct part of the config array and calls saveMtx to set it
 * @method  saveMtxRef
 * @param  {[type]} ref named reference to the called such as "mtx" or "vol"
 * @param  {[type]} x   [description]
 * @param  {[type]} y   [description]
 * @param  {[type]} v   [description]
 * @return {[type]}     [description]
 */
function saveMtxRef(ref, x, y, v)
{	

	switch(ref)
		{
			case "mtx":
				break;
			case "vol":
				saveMtx(config.vol.data, x, y,v);
				break;
			case "pan":
				saveMtx(config.pan.data, x, y,v);
				break
			case "sndA":
				saveMtx(config.sndA.data, x, y,v);
				break;
			case "sndB":
				saveMtx(config.sndB.data, x, y,v);
				break;
			case "stop":
				saveMtx(config.stop.data, x, y,v);
				break;
			case "trkOn":
				saveMtx(config.trkOn.data, x, y,v);
				break;
			case "solo":
				saveMtx(config.solo.data, x, y,v);
				break;
			case "arm":
				saveMtx(config.arm.data, x, y,v);
				break;
			case "up":
				saveMtx(config.up.data, x, y,v);
				break;
			case "down":
				saveMtx(config.down.data, x, y,v);
				break;
			case "left":
				saveMtx(config.left.data, x, y,v);
				break;
			case "right":
				saveMtx(config.right.data, x, y,v);
				break;		
		}
}

/*
* HELPER FUNCTIONS
*/

/*
 * 
 */
/**
 * Grand Central Station
 * Takes care of routing all the data to the correct lighting functions
 * Operation is defined by values in setup and LP.def.*
 * Input takes id of the callback sending the data and the input
 * @for launchpad-eXtend
 * @method  gcs
 * @param  {String} id   caller id, attached in peep functions
 * @param  {Array} args message to process, generally in the form of column, row, id
 * @return {void}      
 */
function gcs(id, args){

	if(id == "sess" || id == "user1" || id == "user2" || id == "mixer")
		config.activebutton = id;
	if(config.activebutton != config.attachTo)
		return;
	if(setupFlag != 1)
		return;
//	post("gcs:", id, " : ",args,"\n");

	if(config.midi.mode == "direct")
	{
		directMidiOut(id,args);
	} 
	else if(config.midi.mode == "custom")
	{
		midiproc(id,args);
	}

	if(config.data.mode == "direct"){
		outlet(1, id,args);
	} else if(config.data.mode == "custom"){
		dataproc(id,args);
	}
	
	if(config.buttonmode == "layers"){
		if(args[1] == 0 || args[0] == "id" || args[1] == "bang")
			return;
		switch(id)
		{
			case "mtx":
				//do the action
				var runme = (asctCoreFunctions[config.activefunc]);
				if(runme != null)
					runme(id, args); //this is a bit of a cheeky tactic to run the function we want by calling it by name
				break;
			case "vol":
				lightOffRef(config.active);//turn off the active light
				config.active = "vol";
				config.activefunc = config.vol.func;
				lightOn(LP.vol, 60);
				loadMtxRef(config.active);
				break;
			case "pan":
				lightOffRef(config.active);
				config.active = "pan";
				config.activefunc = config.pan.func;
				lightOn(LP.pan, 60);
				loadMtxRef(config.active);
				break
			case "sndA":
				lightOffRef(config.active);
				config.active = "sndA";
				config.activefunc = config.sndA.func;
				lightOn(LP.sndA, 60);
				loadMtxRef(config.active);
				break;
			case "sndB":
				lightOffRef(config.active);
				config.active = "sndB";
				config.activefunc = config.sndB.func;
				lightOn(LP.sndB, 60);
				loadMtxRef(config.active);
				break;
			case "stop":
				lightOffRef(config.active);
				config.active = "stop";
				config.activefunc = config.stop.func;
				lightOn(LP.stop, 60);
				loadMtxRef(config.active);
				break;
			case "trkOn":
				lightOffRef(config.active);
				config.active = "trkOn";
				config.activefunc = config.trkOn.func;
				lightOn(LP.trkOn, 60);
				loadMtxRef(config.active);
				break;
			case "solo":
				lightOffRef(config.active);
				config.active = "solo";
				config.activefunc = config.solo.func;
				lightOn(LP.solo, 60);
				loadMtxRef(config.active);
				break;
			case "arm":
				lightOffRef(config.active);
				config.active = "arm";
				config.activefunc = config.arm.func;
				lightOn(LP.arm, 60);
				loadMtxRef(config.active);
				break;
			case "up":
				lightOn(LP.up, 60);
				clearNav(id);
				var runme = (asctCoreFunctions[config.up.func]);
				if(runme != null)
					runme(id, args);
				break;
			case "down":
				lightOn(LP.down, 60);
				clearNav(id);
				var runme = (asctCoreFunctions[config.down.func]);
				if(runme != null)
					runme(id, args);
				break;
			case "left":
				lightOn(LP.left, 60);
				clearNav(id);
				var runme = (asctCoreFunctions[config.left.func]);
				if(runme != null)
					runme(id, args);
				break;
			case "right":
				lightOn(LP.right, 60);
				clearNav(id);
				var runme = (asctCoreFunctions[config.right.func]);
				if(runme != null)
					runme(id, args);
				break;		
		}
	}
}

/**
 * calculate the midi value for the current matrix button
 * @method matrixMidiCalculate 
 * @param  {Array} args standard args
 * @return {void}      
 */
function matrixMidiCalculate(args){
	if(config.midi.mtx.rootpos == "top"){
					var out = args[2]+(args[3]*8);
					out = config.midi.mtx.root+out;
				}
				if(config.midi.mtx.rootpos == "bottom"){
					var out = args[2]+((7-args[3])*8);
					out = config.midi.mtx.root+out;
				}
	return out;
}

/**
 * Direct midi out handling for all buttons
 * @method directMidiOut 
 * @param  {String} id   caller id - mtx, vol, pan etc....
 * @param  {Array} args standard args
 * @return {void}      
 */
function directMidiOut(id, args){
	//config.buttons.right.midi
	//config.buttons.top.midi
	switch(id)
	{
			case "mtx":
				//config.midi.mtx.root = 12;
				//config.midi.mtx.rootpos = "top";
				//value, vel,col,row,
				outlet(0,matrixMidiCalculate(args), args[1]);

				break;
			case "vol":
				outlet(0, config.buttons.right.midi[0], args[1]);
				break;
			case "pan":
				outlet(0, config.buttons.right.midi[1], args[1]);
				break
			case "sndA":
				outlet(0, config.buttons.right.midi[2], args[1]);
				break;
			case "sndB":
				outlet(0, config.buttons.right.midi[3], args[1]);
				break;
			case "stop":
				outlet(0, config.buttons.right.midi[4], args[1]);
				break;
			case "trkOn":
				outlet(0, config.buttons.right.midi[5], args[1]);
				break;
			case "solo":
				outlet(0, config.buttons.right.midi[6], args[1]);
				break;
			case "arm":
				outlet(0, config.buttons.right.midi[7], args[1]);
				break;
			case "up":
				outlet(0, config.buttons.top.midi[0], args[1]);
				break;
			case "down":
				outlet(0, config.buttons.top.midi[1], args[1]);
				break;
			case "left":
				outlet(0, config.buttons.top.midi[2], args[1]);
				break;
			case "right":
				outlet(0, config.buttons.top.midi[3], args[1]);
				break;		

	}
}

/**
 * find a matrix of values by name
 * config.active is useful to scope this
 * @method getMtx
 * @param  {[type]} ref   "vol" "pan" "sndA" etc....
 * @return {int} value in the current matrix space
 * 
 */
function getMtx(ref)
{	
	switch(ref)
		{
			case "mtx":
				return -1;
			case "vol":
				return config.vol.data;
			case "pan":
				return config.pan.data;
			case "sndA":
				return config.sndA.data;
			case "sndB":
				return config.sndB.data;
			case "stop":
				return config.stop.data;
			case "trkOn":
				return config.trkOn.data;
			case "solo":
				return config.solo.data;
			case "arm":
				return config.arm.data;
			case "up":
				return config.up.data;
			case "down":
				return config.down.data;
			case "left":
				return config.left.data;
			case "right":
				return config.right.data;	
		}
}

/**
 * find a specific matrix value by name
 * config.active is useful to scope this
 * @method getMtxRef
 * @param  {[type]} ref   "vol" "pan" "sndA" etc....
 * @return {int} value in the current matrix space
 * 
 */
function getMtxRef(ref,a,i)
{	
	switch(ref)
		{
			case "mtx":
				return -1;
			case "vol":
				return config.vol.data[a][i];
			case "pan":
				return config.pan.data[a][i];
			case "sndA":
				return config.sndA.data[a][i];
			case "sndB":
				return config.sndB.data[a][i];
			case "stop":
				return config.stop.data[a][i];
			case "trkOn":
				return config.trkOn.data[a][i];
			case "solo":
				return config.solo.data[a][i];
			case "arm":
				return config.arm.data[a][i];
			case "up":
				return config.up.data[a][i];
			case "down":
				return config.down.data[a][i];
			case "left":
				return config.left.data[a][i];
			case "right":
				return config.right.data[a][i];	
		}
}

/*
* Get the control surface path, 
* the path defaults to "control_surfaces 1" if this isnt explicitly set
*/
var tempPath = "control_surfaces 1"; //default launchpad id
/**
 * @method path
 * @param  {String} a start of path
 * @param  {String} b end of path
 * @return {void}   
 */
function path(a,b)
{
	tempPath = a+" "+b;
	post("path: ",tempPath,"\n");
	
}

/**
 * delete our launchpad and config objects, you MUST run initialize again after running this function
 * @method free
 * @return {void}
 */
function free()
{
	delete LP;
	delete config;
	post("LP and config deleted");
}

/**
 * Create a 2d array of size 'rows'
 * @method  Create2DArray
 * @param {int} rows number of rows
 * @private
 */
function Create2DArray(rows) {
  var arr = [];
  for (var i=0;i<rows;i++) {
     arr[i] = [];
  }
  return arr;
}
Create2DArray.local - 1;

/**
 * Set the midi root of the matrix according to user settings defined in configSetup()
 * @method  setMtxRoot
 * @param {int} a Value to use as the matrix root
 */
function setMtxRoot(a)
{
	config.midi.mtx.root = a;
	post("matrix root set to:",config.midi.mtx.root,"\n");
}
